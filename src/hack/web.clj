(ns hack.web
  (:require [compojure.core :refer [defroutes GET PUT POST DELETE ANY]]
            [compojure.handler :refer [site]]
            [compojure.route :as route]
            [clojure.java.io :as io]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :refer [redirect response]]
            [ring.adapter.jetty :as jetty]
            [environ.core :refer [env]]))


(def s0 { ;; programming
         :name          "IN_CLASS"
         :next-activity "LUNCH_APPROACHING"
         :notif-type    "SHORT_BUZZ"
         })

(def s1 { ;; lunch approaching so we need to notify
         :name          "LUNCH_APPROACHING"
         :next-activity "LUNCH_SOON"
         :notif-type    "LONG_BUZZ"
         })

(def s2 { ;; counting down
         :name          "LUNCH_SOON"
         :next-activity "COUNTDOWN"
         :notif-type    "VISUAL_COUNTDOWN"
         })

(def s3 { ;; lunch very soon, time to start packing up
         :name          "COUNTDOWN"
         :next-activity "GO_TO_LUNCH"
         :notif-type    "BEEP"
         })

(def s4 { ;; go to lunch!
         :name          "GO_TO_LUNCH"
         :next-activity "NONE"
         :notif-type    "NONE"
         })



(def schedule [s0 s1 s2 s3 s4])


(def pos (atom 0))


(defn get-step-handler
  []
  {:status 200
   :headers {"Content-Type" "application/json"}
   :body (nth schedule @pos)})



(defn get-schedule-handler
  []
  {:status 200
   :headers {"Content-Type" "application/json"}
   :body schedule})



(defn root
  []
  {:status 200
   :headers {"Content-Type" "application/json"}
   :body "Concentration"})


(defn advance!
  []
  (if (> @pos 3)
    (reset! pos 0)
    (swap! pos inc)))


(defn increment-step-handler
  []
  (advance!)
  {:status 200})


(defn step-ui-handler
  []
  (advance!)
  (redirect "/form" :see-other))



(defn reset-handler
  []
  (reset! pos 0)
  (redirect "/form" :see-other))


(defroutes app

  (POST "/step" [] (increment-step-handler))
  (GET "/step" [] (get-step-handler))

  (POST "/step-ui" [] (step-ui-handler))
  (POST "/reset" [] (reset-handler))

  (GET "/form" [] (route/not-found (slurp (io/resource "form.html"))))
  (GET "/schedule" [] (get-schedule-handler))
  (ANY "*" []
    (root)))


(comment (route/not-found (slurp (io/resource "404.html"))))


(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 5000))]
    (jetty/run-jetty (wrap-json-response (site #'app)) {:port port :join? false})))



;; For interactive development:
;; (.stop server)
;; (def server (-main))
